module gitlab.com/hugo-blog/hugo-module-gdpr-privacy

go 1.19

require gitlab.com/hugo-blog/hugo-module-meta v0.6.2

require gitlab.com/hugo-blog/hugo-module-i18n v0.2.1
