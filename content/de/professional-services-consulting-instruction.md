## Geschäftsmäßige Beratungs- und Schulungsdienstleistungen

### Beschreibung und Umfang der Datenverarbeitung

Ich verarbeite ausschließlich außerhalb meines Online-Angebots die geschäftsbezogenen Daten meiner Kunden im Rahmen meiner vertraglichen Leistungen zu denen konzeptionelle und strategische Beratung, Software- und Designentwicklung, Software-Beratung, Umsetzung von Software und Prozessen, Datenanalyse, Beratungsleistungen und Schulungsleistungen gehören.

Hierbei verarbeite ich
1. Bestandsdaten (beispielsweise Kundenstammdaten, wie Namen oder Adressen)
1. Kontaktdaten (beispielsweise E-Mail, Telefonnummern)
1. Inhaltsdaten (beispielsweise Quellcode, Texte, Zeichnungen, Diagramme, Fotografien, Videos)
1. Vertragsdaten (beispielsweise Vertragsgegenstand, Laufzeit)
1. Zahlungsdaten (beispielsweise Bankverbindung, Zahlungsziele, Zahlungshistorie)
1. Nutzungs- und Metadaten (zum Beispiel im Rahmen der Auswertung und Erfolgsmessung von Marketingmaßnahmen)

Besondere Kategorien personenbezogener Daten verarbeite ich grundsätzlich nicht, außer wenn diese ausdrücklich Bestandteile einer beauftragten Verarbeitung sind.

### Rechtsgrundlage für die Datenverarbeitung

Betroffene Personen können meine Kunden, Interessenten, Lieferanten, potentielle Geschäftspartner sowie deren Kunden, Nutzer, Websitebesucher oder Mitarbeiter sowie Dritte sein.

Bei der Verarbeitung von personenbezogenen Daten, die zur Erfüllung eines Vertrages erforderlich ist, dessen Vertragspartei Sie als betroffene Person sind, bildet Artikel 6 Absatz 1 Buchstabe b DSGVO die Rechtsgrundlage.

Für Zwecke der Analyse, Statistik, Optimierung sowie zur Durchführung von Sicherheitsmaßnahmen ist Artikel 6 Absatz 1 Buchstabe f DSGVO die Rechtsgrundlage.

#### Zweck der Datenverarbeitung

Der Zweck der Verarbeitung besteht in der Erbringung von Vertragsleistungen, der Abrechnung und meinem Kundenservice.

Ich verarbeite Daten, die zur Begründung und Erfüllung der vertraglichen Leistungen erforderlich sind und weise auf die Erforderlichkeit ihrer Angabe hin. Eine Offenlegung an Dritte erfolgt nur, wenn sie im Rahmen eines Auftrags erforderlich ist. Bei der Verarbeitung der mir im Rahmen eines Auftrags überlassenen Daten handle ich entsprechend den Weisungen meiner Auftraggeber sowie der gesetzlichen Vorgaben einer Auftragsverarbeitung gemäß Artikel 28 DSGVO und verarbeite die Daten zu keinen anderen als den auftragsgemäßen Zwecken.

### Dauer der Speicherung

Ich lösche die Daten nach Ablauf gesetzlicher Gewährleistungs- und vergleichbarer Pflichten.
Ich überprüfe die Erforderlichkeit alle zwei Jahre.
Unterliegen Daten den gesetzlichen Archivierungspflichten, lösche ich sie Ablauf der gesetzlichen Aufbewahrungsdauer (6 Jahre gemäß § 257 Absatz 1 HGB, 10 Jahre gemäß § 147 Absatz 1 AO).
Wenn Sie mir gegenüber im Rahmen eines geschäftlichen Auftrags Daten offengelegt haben, lösche ich die Daten entsprechend den Vorgaben des Auftrags, grundsätzlich nach Ende des Auftrags.

### Widerspruchs- und Beseitigungsmöglichkeit

Widersprechen Sie als Auftraggeber der Verarbeitung der für die Erbringung meiner Vertragsleistung notwendigen Daten, kann ich die Geschäftsbeziehung mit Ihnen nicht fortführen. Unbeschadet dessen gelten die allgemeinen Regeln zur Datenlöschung und -sperrung.
