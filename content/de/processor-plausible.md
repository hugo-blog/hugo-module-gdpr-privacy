### Analyse-Plattform

Für die vollständig anonymisierte Auswertung der Nutzung meines Angebots habe ich ein Unternehmen beauftragt, das eine datenschutzfreundliche Analyseplattform betreibt.
Ihre Daten werden nicht dorthin übertragen, wenn Ihr Browser ein [„do-not-track“](https://de.wikipedia.org/wiki/Do_Not_Track_(Software)) oder ein [„Global Privacy Control“](https://globalprivacycontrol.org/)-Signal sendet.

Der Dienst [plausible.io](https://plausible.io/) wird angeboten von

Plausible Insights OÜ  
Västriku tn 2  
50403 Tartu  
Estland

Registerbehörde: [e-Business Register](https://www.rik.ee/en/e-business-register)  
Registernummer: [14709274](https://ariregister.rik.ee/eng/company/14709274/O%C3%9C-Plausible-Insights)  

Der Auftragsverarbeiter speichert und verarbeitet die anonymisierten Daten in einem Rechenzentrum in [Frankfurt (Main), Deutschland](https://plausible.io/blog/made-in-eu).
