## Sachliche Kurzfassung

Innerhalb der Europäischen Union (EU) gelten strenge Datenschutzvorschriften.
Deshalb muss ich Ihnen genau erklären, wie ich Ihre Daten verarbeite.
Außerdem erkläre ich Ihnen Ihre Rechte in diesem Zusammenhang.
Um Zeit zu sparen, lesen Sie zuerst die Kurzfassung in Stichworten:

- Verantwortlich für dieses Online-Angebot im Sinne der EU-DSGVO und nationaler Gesetzgebung ist {{< meta/full-name >}}.
- Ich begrüße die Rechtsvorschriften der EU-DSGVO sowie das Prinzip der Datensparsamkeit.
- Ich untersuche, wie mein Publikum mein Angebot insgesamt nutzt.
  Dabei bleiben Sie als einzelne Person unerkannt.
  Ich setze keine Technik ein, mit der ich Ihr persönliches Verhalten beobachten könnte. Ich benutze keine sogenannten _Cookies_.
- Sie unterstützen mich, wenn Sie diese moderne Form der Analyse zulassen.
- Sie können mir freiwillig E-Mails schicken.
  Auch meinen Newsletter können Sie ohne Zwang oder Verpflichtung bestellen.
  Sie können den Newsletter jederzeit kündigen.
- Ihre Daten gebe ich nur dann an Dritte weiter, wenn ich per Gesetz oder gültigen Gerichtsbeschluss dazu verpflichtet bin.
- Ihre Daten werden nur innerhalb der EU oder in Drittländern mit [gleichwertigem Schutz](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en) verarbeitet.
