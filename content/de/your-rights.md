## Rechte der betroffenen Person
Werden Ihre personenbezogenen Daten verarbeitet, sind Sie betroffene Person im Sinne der DSGVO und es stehen Ihnen folgende Rechte gegenüber mir, der verantwortlichen Person zu:

### Auskunftsrecht
Sie können gemäß Artikel 15 DSGVO von mir als verantwortlicher Person eine Bestätigung darüber verlangen, ob ich personenbezogene Daten verarbeite, die Sie betreffen.
Liegt eine solche Verarbeitung vor, können Sie von der verantwortlichen Person über folgende Informationen Auskunft verlangen:

1. die Zwecke, zu denen die Daten verarbeitet werden
1. die Kategorien von Daten, welche verarbeitet werden
1. die empfangenden Stellen oder die Kategorien von empfangenden Stellen, denen gegenüber die Sie betreffenden Daten offengelegt wurden oder noch offengelegt werden
1. die geplante Dauer der Speicherung der Sie betreffenden Daten oder, falls konkrete Angaben hierzu nicht möglich sind, Kriterien für die Festlegung der Speicherdauer
1. das Bestehen eines Rechts auf Berichtigung oder Löschung der Sie betreffenden Daten, eines Rechts auf Einschränkung der Verarbeitung durch die verantwortliche Person oder eines Widerspruchsrechts gegen diese Verarbeitung
1. das Bestehen eines Beschwerderechts bei einer Aufsichtsbehörde gemäß Artikel 77 DSGVO
1. alle verfügbaren Informationen über die Herkunft der Daten, wenn die Daten nicht bei Ihnen als betroffene Person direkt erhoben werden

Sie können Auskunft darüber verlangen, ob ich Ihre Daten in ein Drittland oder an eine internationale Organisation übermittle.
Im Zusammenhang mit dieser Übermittlung können Sie verlangen, über die geeigneten Garantien gemäß Artikel 46 DSGVO  unterrichtet zu werden.

### Recht auf Berichtigung
Sie haben gemäß Artikel 16 DSGVO ein Recht auf Berichtigung und Vervollständigung gegenüber der verantwortlichen Person, sofern die verarbeiteten Daten, die Sie betreffen, unrichtig oder unvollständig sind.
Die verantwortliche Person hat die Berichtigung unverzüglich vorzunehmen.

### Recht auf Löschung
_Löschpflicht_  
Sie können gemäß Artikel 17 DSGVO von mir als verantwortlicher Person verlangen, dass Ihre Daten unverzüglich gelöscht werden, und ich bin verpflichtet, diese Daten unverzüglich zu löschen, sofern einer der folgenden Gründe zutrifft:

1. Die Sie betreffenden Daten sind für die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr notwendig.
1. Sie widerrufen Ihre Einwilligung, auf die sich die Verarbeitung gemäß Artikel 6 Absatz 1 Buchstabe a oder Artikel 9 Absatz 2 Buchstabe a DSGVO stützte, und es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung.
1. Sie legen gemäß Artikel 21 Absatz 1 DSGVO Widerspruch gegen die Verarbeitung ein und es liegen keine vorrangigen berechtigten Gründe für die Verarbeitung vor, oder Sie legen gemäß Artikel 21 Absatz 2 DSGVO Widerspruch gegen die Verarbeitung ein.
1. Die Sie betreffenden Daten wurden unrechtmäßig verarbeitet.
1. Die verantwortliche Person ist nach dem Unionsrecht oder dem Recht der Mitgliedstaaten, dem sie unterliegt, verpflichtet, die Sie betreffenden Daten zu löschen.
1. Die Sie betreffenden Daten wurden in Bezug auf angebotene Dienste der Informationsgesellschaft gemäß Artikel 8 Absatz 1 DSGVO erhoben.

_Information an Dritte_  
Habe ich als verantwortliche Person die Sie betreffenden Daten öffentlich gemacht und bin ich gemäß Artikel 17 Absatz 1 DSGVO zu deren Löschung verpflichtet, so treffe ich unter Berücksichtigung der verfügbaren Technologie und der Implementierungskosten angemessene Maßnahmen. Das können auch rein technische Maßnahmen sein.
Mit den Maßnahmen informiere ich die anderen im Sinne der DSGVO für die Verarbeitung Ihrer Daten verantwortlichen Personen darüber, dass Sie als betroffene Person von ihnen die Löschung aller Verknüpfungen zu diesen Daten oder von Kopien oder Replikationen dieser Daten verlangt haben.

_Ausnahmen_  
Sie haben kein Recht auf Löschung, wenn die Verarbeitung erforderlich ist

1. zur Ausübung des Rechts auf freie Meinungsäußerung und Information
1. zur Erfüllung einer rechtlichen Verpflichtung, die die Verarbeitung nach dem Recht der Union oder der Mitgliedstaaten, dem die verantwortliche Person unterliegt, erfordert, oder zur Wahrnehmung einer Aufgabe, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die der verantwortlichen Person übertragen wurde
1. aus Gründen des öffentlichen Interesses im Bereich der öffentlichen Gesundheit gemäß Artikel 9 Absatz 2 Buchstabe h und i sowie Artikel 9 Absatz 3 DSGVO
1. für im öffentlichen Interesse liegende Archivzwecke, wissenschaftliche oder historische Forschungszwecke oder für statistische Zwecke gemäß Artikel 89 Absatz 1 DSGVO, soweit das unter Abschnitt 1) genannte Recht voraussichtlich die Verwirklichung der Ziele dieser Verarbeitung unmöglich macht oder ernsthaft beeinträchtigt, oder
1. zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung eingeschränkt.
Das bedeutet, dass die Daten gesperrt werden und nicht für andere Zwecke verarbeitet werden.
Das gilt beispielsweise für Daten, die aus handels- oder steuerrechtlichen Gründen aufbewahrt werden müssen.  
Nach gesetzlichen Vorgaben in Deutschland erfolgt die Aufbewahrung insbesondere für 10 Jahre gemäß § 147 Absatz 1 AO und § 257 Absatz 1 Nr. 1 und 4, Absatz 4 HGB (Bücher, Aufzeichnungen, Lageberichte, Buchungsbelege, Handelsbücher, für Besteuerung relevante Unterlagen und Ähnliches) und 6 Jahre gemäß § 257 Absatz 1 Nr. 2 und 3, Absatz 4 HGB (Handelsbriefe).
**E-Mails stellen Handelsbriefe dar, sofern sie im Rahmen einer geschäftlichen Beziehung zwischen Ihnen als betroffener Person und mir als verantwortlicher Person ausgetauscht wurden.**

### Recht auf Einschränkung der Verarbeitung
Wenn mindestens eine der folgenden Voraussetzungen erfüllt ist, können Sie die Einschränkung der Verarbeitung der Sie betreffenden Daten gemäß Artikel 18 DSGVO verlangen:

1. Wenn Sie die Richtigkeit der Sie betreffenden Daten für eine Dauer bestreiten, die es der verantwortlichen Person ermöglicht, die Richtigkeit der Daten zu überprüfen.
1. Die Verarbeitung ist unrechtmäßig und Sie lehnen die Löschung der Daten ab und verlangen stattdessen die Einschränkung der Nutzung der Daten.
1. Die verantwortliche Person benötigt die Daten nicht länger für die Zwecke der Verarbeitung, Sie selbst benötigen diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.
1. Sie haben Widerspruch gegen die Verarbeitung gemäß Artikel 21 Absatz 1 DSGVO eingelegt und es steht noch nicht abschließend fest, ob die berechtigten Gründe der verantwortlichen Person gegenüber Ihren Gründen überwiegen.

Wurde die Verarbeitung der Sie betreffenden Daten eingeschränkt, dürfen diese Daten – von ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet werden.
Wurde die Einschränkung der Verarbeitung nach den oben genannten Voraussetzungen eingeschränkt, muss die verantwortliche Person Sie unterrichten, bevor die Einschränkung aufgehoben wird.

### Recht auf Unterrichtung
Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber der verantwortlichen Person geltend gemacht, ist diese gemäß Artikel 19 DSGVO verpflichtet, allen empfangenden Stellen diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, denen die Sie betreffenden Daten offengelegt wurden, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand verbunden.  
Sie haben gegenüber der verantwortlichen Person das Recht, über diese empfangenden Stellen unterrichtet zu werden.

### Recht auf Datenübertragbarkeit
Sie haben gemäß Artikel 20 DSGVO das Recht, die Sie betreffenden Daten, die Sie der verantwortlichen Person bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten.
Außerdem haben Sie das Recht, diese Daten einer anderen verantwortlichen Person ohne Behinderung durch die verantwortliche Person, der die Daten bereitgestellt wurden, zu übermitteln, sofern

1. die Verarbeitung auf einer Einwilligung gemäß Artikel 6 Absatz 1 Buchstabe a DSGVO oder Artikel 9 Absatz 2 Buchstabe a DSGVO oder auf einem Vertrag gemäß Artikel 6 Absatz 1 Buchstabe b DSGVO beruht und
1. die Verarbeitung mithilfe automatisierter Verfahren erfolgt.

In Ausübung dieses Rechts haben Sie ferner das Recht, zu erwirken, dass die Sie betreffenden Daten direkt von einer verantwortlichen Person einer anderen verantwortlichen Person übermittelt werden, soweit dies technisch machbar ist.  
Freiheiten und Rechte anderer Personen dürfen hierdurch nicht beeinträchtigt werden.  
Das Recht auf Datenübertragbarkeit gilt nicht für eine Verarbeitung von Daten, die für die Wahrnehmung einer Aufgabe erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die der verantwortlichen Person übertragen wurde.

### Widerspruchsrecht
Sie haben gemäß Artikel 21 DSGVO das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden Daten, die aufgrund von Artikel 6 Absatz 1 Buchstabe e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling.  
In diesem Fall darf die verantwortliche Person die Sie betreffenden Daten nicht mehr verarbeiten, es sei denn, sie kann zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.
