### Content Delivery Network CDN77

Zur Auslieferung meines Online-Angebots bediene ich mich eines „Content Delivery Networks“.
Dabei werden meine Inhalte weltweit auf die Server des Auftragsverarbeiters DataCamp übertragen und von dort an Sie ausgeliefert.

Der Dienst [CDN77](https://www.cdn77.com/contact) wird angeboten von

DataCamp Limited  
207 Regent Street  
London W1B HH  
Vereinigtes Königreich
    
Registerbehörde: [Companies House](https://www.gov.uk/government/organisations/companies-house)  
Firmennummer: [07489096](https://find-and-update.company-information.service.gov.uk/company/07489096)

Ich habe die Standorte für mein Online-Angebot so ausgewählt und beschränkt, dass die Inhalte nur aus EU-Mitgliedstaaten und solchen Nicht-EU-Ländern ausgeliefert werden, die aus Sicht der EU-Kommission ein vergleichbares Schutzniveau bieten.
Ich lasse meine Inhalte von folgenden Standorten aus ausliefern:

{{< _library/hugo-module-gdpr-privacy/pointsOfPresence "cdn77" >}}
