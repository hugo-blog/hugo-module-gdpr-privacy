## Einleitung

Mir ist es wichtig, Ihre persönlichen Daten zu schützen und sicher zu verarbeiten.
Ich halte mich selbstverständlich an die Bestimmungen der relevanten Datenschutzgesetze, besonders an die [EU-Datenschutzgrundverordnung (DSGVO)](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32016R0679).
Ihre personenbezogenen Daten nenne ich im folgenden Text einfach und kurz „Daten“. Personenbezogene Daten sind alle Daten, mit denen Sie persönlich identifiziert werden können.
Alle verbundenen Webseiten, Funktionen und Inhalte, die ich Ihnen online anbiete, nenne ich gemeinsam kurz „Online-Angebot“.

Ich erkläre Ihnen, wie ich Ihre Daten verarbeite, wenn Sie mein Online-Angebot nutzen.

Grundsätzlich verfechte ich das Prinzip der Datensparsamkeit. Ich erhebe und verarbeite also nur Daten für eindeutige und konkret benannte Zwecke. Diese Zwecke mache ich für Sie sichtbar. Wann immer es technisch möglich ist, bitte ich Sie um Ihr Einverständnis, bevor ich Ihre Daten verarbeite.
Sie können mein Angebot im Rahmen des technisch Möglichen unerkannt nutzen.
Ihre persönlichen Daten werden verarbeitet, wenn es sachlich oder technisch nötig ist. Außerdem werden Ihre Daten verarbeitet, wenn ein Gesetz dies von mir verlangt.
