### Content Delivery Network KeyCDN

Zur Auslieferung meines Online-Angebots bediene ich mich eines „Content Delivery Networks“.
Dabei werden meine Inhalte weltweit auf die Server des Auftragsverarbeiters KeyCDN übertragen und von dort an Sie ausgeliefert.

Der Dienst [KeyCDN](https://www.keycdn.com/about) wird angeboten von

proinity LLC  
Reichenauweg 1  
8272 Ermatingen  
Schweiz

Registerbehörde: [Bundesamt für Statistik](https://www.bfs.admin.ch/bfs/de/home/register/unternehmensregister.html)  
UID: [CHE-459.847.656](https://www.uid.admin.ch/Detail.aspx?uid_id=CHE-459.847.656)

Ich habe die Standorte für mein Online-Angebot so ausgewählt und beschränkt, dass die Inhalte nur aus EU-Mitgliedstaaten und solchen Nicht-EU-Ländern ausgeliefert werden, die aus Sicht der EU-Kommission ein vergleichbares Schutzniveau bieten.
Ich lasse meine Inhalte von folgenden Standorten aus ausliefern:

{{< _library/hugo-module-gdpr-privacy/pointsOfPresence "keycdn" >}}
