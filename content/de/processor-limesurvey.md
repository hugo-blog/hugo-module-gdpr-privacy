### Online-Umfragen

Für die Durchführung von Online-Umfragen habe ich das Unternehmen LimeSurvey beauftragt.
Sofern Sie an Online-Umfragen teilnehmen, die ich dort erstellt habe, stimmen den [Datenschutzbestimmungen](https://www.limesurvey.org/de/datenschutzhinweise) und [Allgemeinen Geschäftsbedingungen](https://www.limesurvey.org/de/agb) dieses Unternehmens zu.
Vor Beginn der Umfrage werden Sie jeweils explizit um Ihr Einverständnis gebeten.

Der Dienst [LimeSurvey](https://www.limesurvey.org/de) wird angeboten von der

LimeSurvey GmbH  
Umfragedienste & Beratung  
Papenreye 63  
22453 Hamburg  
Deutschland
    
Registergericht: Amtsgericht Hamburg  
Registernummer: HRB 137625  
USt-IdNr.: DE301233134

Mit dem Auftragsverarbeiter wurde vertraglich vereinbart, dass Ihre Daten ausschließlich an einem Standort in Deutschland gespeichert und verarbeitet werden.
