## E-Mail Kontakt

### Beschreibung und Umfang der Datenverarbeitung

Sie können über die bereitgestellten E-Mail-Adressen Kontakt mit mir aufnehmen.
Unabhängig davon können Sie meinen Newsletter abonnieren.
Ich werde Ihnen den Newsletter nur dann zusenden, wenn Sie ausdrücklich darum gebeten haben. Wenn Sie nur Kontakt aufnehmen, erhalten Sie meinen Newsletter nicht automatisch.
Die Zusendung von E-Mails Ihrerseits ist ebenso freiwillig wie das Abonnement meines Newsletters.
Insofern erklären Sie sich allein durch die Zusendung einer E-Mail an mich damit einverstanden, dass ich die in der E-Mail enthaltenen Daten in vollem Umfang nutzen und verarbeiten darf.
In diesem Fall werden Ihre mit der E-Mail übermittelten personenbezogenen Daten verarbeitet.

Ich gebe in diesem Zusammenhang keine Daten an Dritte weiter.
Ihre Daten verwende ich ausschließlich für die Verarbeitung der Konversation.

Wenn Sie meinen Newsletter abonnieren, sende ich einen Link zur Bestätigung.
Diesen Link sende ich zu der E-Mail-Adresse, an die ich den Newsletter zustellen soll. Sie bestätigen, dass Sie Kontrolle über diese E-Mail-Adresse haben, wenn Sie den Link im Browser öffnen.
Außerdem bestätigen Sie, dass Sie meinen Newsletter tatsächlich haben möchten und ihn selbst bestellt haben.
Dieses Verfahren wird auch „Double-Opt-in“ genannt.
Ich habe ein spezialisiertes Unternehmen beauftragt, den Newsletter zu versenden.
Dort wird Ihre E-Mail-Adresse nur verwendet, um Ihnen den Newsletter zu schicken.
Dem im Auftrag verarbeitenden Unternehmen ist es vertraglich und gesetzlich strikt verboten, Ihre Daten anders zu nutzen.
Insbesondere darf das Unternehmen Ihre Daten keinem Dritten offenbaren.
Sollte ich erfahren, dass das Unternehmen versucht, Ihre Daten zu verkaufen, werde ich Strafanzeige erstatten, den Vertrag mit dem Unternehmen lösen und Schadenersatzforderungen geltend machen.

Ausgenommen hiervon ist ein Missbrauch meiner E-Mail-Adressen oder meines Newsletter-Angebots.
In diesem Fall werde ich meine Rechte durchsetzen.
Notfalls werde ich rechtliche Schritte einleiten.
Zu diesem Zweck werde ich die notwendigen Daten meinem Rechtsbeistand, zuständigen Polizeibehörden, Gerichten, Finanzämtern und anderen berechtigten öffentlichen Stellen zur Verfügung stellen.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die Verarbeitung der Daten, die im Zuge einer Übersendung einer E-Mail übermittelt werden, ist Artikel 6 Absatz 1 Buchstabe f DSGVO.

Zielt der E-Mail-Kontakt auf den Abschluss eines Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Artikel 6 Absatz 1 Buchstabe b DSGVO.

Stellte der E-Mail-Kontakt eine missbräuchliche Nutzung meines Online-Angebots dar, so ist die zusätzliche Rechtsgrundlage für die Verarbeitung und Weitergabe im Rahmen der rechtmäßigen gerichtlichen oder außergerichtlichen Durchsetzung meiner Interessen und Rechte Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Zweck der Datenverarbeitung

Ich verarbeite die Daten, um auf Ihre Kontaktaufnahme zu antworten, oder um Ihnen regelmäßigen einen Newsletter zu schicken.
Falls Sie mir zur Kontaktaufnahme eine E-Mail zugesendet haben, liegt hieran auch das erforderliche berechtigte Interesse an der Verarbeitung der Daten.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind.
Für die Daten, die Sie per E-Mail übersandt haben, ist dies dann der Fall, wenn die jeweilige Konversation mit Ihnen beendet ist.
Beendet ist die Konversation dann, wenn sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist.
Für die Zusendung des Newsletters ist dies dann der Fall, wenn Sie das Abonnement gekündigt haben oder ich erkennen kann, dass Sie das Abonnement mehr als ein Jahr lang nicht mehr aktiv genutzt haben.
Ich überprüfe die Erforderlichkeit alle zwei Jahre.
Zudem gelten die gesetzlichen Archivierungspflichten.

### Widerspruchs- und Beseitigungsmöglichkeit

Nehmen Sie per E-Mail Kontakt mit mir auf, so können Sie der Speicherung Ihrer Daten jederzeit widersprechen. In einem solchen Fall kann ich die Konversation mit Ihnen leider nicht fortsetzen.

Sie können Ihren Widerruf entweder ebenfalls per E-Mail oder telefonisch oder per Brief an mich als verantwortliche Person richten. Meine E-Mail-Adresse, meine Telefonnummer und meine Anschrift finden Sie am Anfang dieses Dokuments.

Alle Daten, die im Zuge der Kontaktaufnahme gespeichert wurden, werden in diesem Fall nach Prüfung der Rechtmäßigkeit der Löschung mit einer angemessenen Frist gelöscht. Ist eine Löschung rechtlich nicht möglich oder die Rechtmäßigkeit zweifelhaft, tritt an ihre Stelle eine Sperrung. Dies ist insbesondere dann der Fall, wenn die Kontaktaufnahme eine missbräuchliche Nutzung des Online-Angebots darstellt oder sich aus der Kontaktaufnahme ein Rechtsstreit zwischen der betroffenen Person und der verantwortlichen Person ergibt oder ergeben könnte.
