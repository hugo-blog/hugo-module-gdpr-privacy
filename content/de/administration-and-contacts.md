## Administration, Finanzbuchhaltung, Kontaktverwaltung

### Beschreibung und Umfang der Datenverarbeitung

Im Rahmen von Verwaltungsaufgaben sowie zur Organisation meiner Unternehmung, Finanzbuchhaltung und zur Befolgung der gesetzlichen Pflichten (wie unter anderem der Archivierung) verarbeite ich Daten ausschließlich außerhalb meines Online-Angebots. In diesem Zusammenhang verarbeite ich dieselben Daten, die ich verarbeite, wenn ich vertraglich vereinbarte berufliche Dienstleistungen erbringe.

In diesem Kontext offenbare oder übermittle ich Daten an die Finanzverwaltung, beratende Personen und Unternehmen (wie Steuerberatungskanzleien oder Wirtschaftsprüfungsgesellschaften) sowie weitere Gebührenstellen, öffentliche Stellen und Dienstleistungsunternehmen zur Abwicklung von Zahlungen.

Ferner speichere ich auf Grundlage meiner betriebswirtschaftlichen Interessen Angaben zu Lieferfirmen, sonstigen geschäftlichen Kontakten und potenziellen geschäftlichen Verbindungen.

Betroffene Personen können meine Kundschaft, Interessierte, Lieferfirmen, potenzielle Geschäftsverbindungen sowie deren Kundschaft, Publikum, Leserschaft oder Personal sowie Dritte sein.

### Rechtsgrundlage für die Datenverarbeitung

Wenn ich Daten verarbeite, um meine Geschäftstätigkeit auszuüben oder vertraglich vereinbarte Leistungen zu erbringen, sind die Rechtsgrundlagen der Verarbeitung Artikel 6 Absatz 1 Buchstabe c DSGVO und Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Zweck der Datenverarbeitung

Der Zweck und mein Interesse an der Verarbeitung liegt in der Administration, Finanzbuchhaltung und Archivierung von Daten.
Das sind Aufgaben, mit denen ich meine Geschäftstätigkeit aufrechterhalte. Darunter fallen teilweise gesetzlich vorgeschriebene Aufgaben, die ich  erfüllen muss. Außerdem sind dies teilweise Aufgaben, mit denen ich meine Leistungen erbringen kann.

### Dauer der Speicherung

Die mehrheitlich unternehmensbezogenen Daten speichere ich grundsätzlich dauerhaft.
Um gesetzliche Archivierungspflichten zu erfüllen, lösche ich die Daten frühestens nach dem Ablauf der gesetzlichen Aufbewahrungsfristen und spätestens ein Jahr nach dem Ablauf dieser Fristen.

### Widerspruchs- und Beseitigungsmöglichkeit

Einen Widerspruch können Sie schriftlich an meine postalische Adresse als verantwortliche Person richten.
In den meisten Fällen dürfte mein Interesse als verantwortliche Person an der Verarbeitung Ihrem Verlangen auf Löschung überwiegen.
In solchen Fällen werden die Daten regelmäßig gesperrt anstatt gelöscht.
Unbeschadet dessen gelten die allgemeinen Regeln zur Datenlöschung und -sperrung.
