## Allgemeines zur Datenverarbeitung

### Umfang der Verarbeitung personenbezogener Daten

Ich verarbeite Ihre Daten grundsätzlich nur, damit Sie mein Online-Angebot nutzen können und ich meine Inhalte präsentieren und meine Leistungen erbringen kann.
Ich verarbeite Ihre Daten regelmäßig nur, wenn Sie vorher einwilligen.
Eine Ausnahme gilt, wenn Sie aus tatsächlichen Gründen nicht vorab einwilligen können und gesetzliche Vorschriften mir gestatten, die Daten trotzdem zu verarbeiten.

### Rechtsgrundlage für die Verarbeitung personenbezogener Daten

Sie können als betroffene Person einwilligen, dass ich Ihre Daten verarbeite.
Dann bildet Artikel 6 Absatz 1 Buchstabe a DSGVO die Rechtsgrundlage.

Wenn Sie als betroffene Person Vertragspartei sind und ich Ihre Daten verarbeiten muss, damit der Vertrag erfüllt werden kann, bildet Artikel 6 Absatz 1 Buchstabe b DSGVO die Rechtsgrundlage.
Dies gilt auch, wenn ich Ihre Daten verarbeite, um Verträge mit Ihnen zu verhandeln, anzubahnen oder vorzubereiten.

In einigen Fällen bin ich als Unternehmer oder selbstständig Erwerbstätiger rechtlich verpflichtet, Ihre Daten zu verarbeiten.
Dann bildet Artikel 6 Absatz 1 Buchstabe c DSGVO die Rechtsgrundlage.

Manchmal erfordern lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person, dass ich Daten verarbeite.
Dann ist Artikel 6 Absatz 1 Buchstabe d DSGVO die Rechtsgrundlage.

Es ist möglich, dass ich selbst als Person, meine Unternehmung oder Dritte ein berechtigtes Interesse haben, dass ich Daten verarbeite.
Wenn die Interessen, Grundrechte und Grundfreiheiten der betroffenen Person dieses Interesse nicht überwiegen, so bildet Artikel 6 Absatz 1 Buchstabe f DSGVO die Rechtsgrundlage für die Verarbeitung.
Diese Rechtsgrundlage verwende ich sparsam.

### Datenlöschung und Speicherdauer

Ich sperre oder lösche die Daten der betroffenen Person, sobald der Zweck der Speicherung entfällt.
Als verantwortliche Person unterliege ich der europäischen und nationalen Gesetzgebung.
Ich verarbeite Daten, wenn diese Gesetzgebung in unionsrechtlichen Verordnungen, Gesetzen oder sonstigen Vorschriften es vorsieht.
Ich sperre oder lösche die Daten auch dann, wenn eine durch diese Rechtsnormen vorgeschriebene Speicherfrist abläuft.
Die Daten werden trotzdem weiter gespeichert, wenn es nötig ist, um einen Vertrag abzuschließen oder zu erfüllen.
