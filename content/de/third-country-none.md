## Keine Übermittlung in Drittländer oder an internationale Organisationen
Aufgrund der komplizierten Auflagen und Bedingungen der Artikel 44 ff. DSGVO findet weder eine Verarbeitung von Daten in einem Drittland noch eine Übermittlung von Daten in ein Drittland oder an internationale Organisationen statt.
