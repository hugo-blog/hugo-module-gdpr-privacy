## Begriffsklärung

_„Personenbezogene Daten“_ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen. Solche Personen bezeichne ich im Folgenden „betroffene Person“.
Als identifizierbar wird eine _natürliche Person_ angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (unter anderem sogenannte Cookies) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann.
Diese Merkmale kennzeichnen die physische, physiologische, genetische, psychische, wirtschaftliche, kulturelle oder soziale Identität einer natürlichen Person.

_„Verantwortliche Person“_ (im Gesetzestext: _„Verantwortlicher“_) bezeichnet die natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet.

_„Verarbeitung“_ ist jeder ausgeführte Vorgang oder jede Reihung solcher Vorgänge im Zusammenhang mit personenbezogenen Daten. Dabei spielt es keine Rolle, ob die Vorgänge aus automatisierten oder manuellen Verfahren bestehen.
Der Begriff ist weitreichend und umfasst praktisch jede Art und Form des Umgangs mit Daten.

_„Drittland“_ (oder Drittstaat) ist jeder Staat oder jedes unabhängige Rechtsgebiet außerhalb des Europäischen Wirtschaftsraums (EWR).

_„Internationale Organisation“_ ist jede Organisation, die Geschäftsstellen in einem Drittland unterhält oder zu Teilen der Gesetzgebung eines Drittlands unterworfen ist.

Unternehmen oder Personen, _„die im Auftrag verarbeiten“_ (im Gesetzestext: _„Auftragsverarbeiter“_) sind Dritte, die für eine verantwortliche Person im Auftrag Daten verarbeiten.
Dritte, die im Auftrag verarbeiten, müssen den Anweisungen der verantwortlichen Person folgen.
Darüber muss die verantwortliche Person mit jedem im Auftrag verarbeitenden Unternehmen jeweils einen Vertrag schließen.
Diese Verträge müssen bestimmte Anforderungen der EU-DSGVO erfüllen.
Sie müssen nach EU-Recht oder nach dem Recht eines EU-Mitgliedsstaates geschlossen werden.

_„IP-Adresse“_ ist eine eindeutige Kennung im Netzwerk.
Das Dienstleistungsunternehmen für Ihren Internetzugang weist Ihnen diese Kennung zu.
Die zeitliche Zuordnung zwischen dieser Kennung und Ihnen als Person muss unter bestimmten Umständen vom Dienstleistungsunternehmen über einen längeren Zeitraum gespeichert werden.
Dadurch können Behörden Ihre Aktivitäten unter Umständen im Rahmen strafrechtlicher oder anderer Ermittlungen auch nachträglich rekonstruieren.
