### Newsletter-Versand

Für den regelmäßigen Versand meines E-Mail-Newsletters bediene ich mich eines professionellen, mehrfach zertifizierten Versanddienstleisters.

Der Dienst [rapidmail](https://www.rapidmail.de/) wird angeboten von der

rapidmail GmbH  
Augustinerplatz 2  
79098 Freiburg im Breisgau  
Deutschland
    
Registergericht: Freiburg i.Br.  
Registernummer: HRB 706983  
USt-IdNr.: DE262810345

Der Auftragsverarbeiter sichert vertraglich zu, dass Ihre Daten ausschließlich an einem Standort in Deutschland gespeichert und verarbeitet werden.
