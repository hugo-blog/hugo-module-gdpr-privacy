### E-Mail-Versand

Für den Versand von persönlichen und transaktionalen E-Mails bediene ich mich eines professionellen Versanddienstleisters.

Der Dienst [SMTP2GO](https://www.smtp2go.com/about/) wird angeboten von

SAND DUNE MAIL LIMITED  
96-106 Manchester Street  
Christchurch 8011  
Neuseeland

Registerbehörde: [New Zealand Companies Office](https://www.companiesoffice.govt.nz/)  
Unternehmensnummer: [1842323](https://app.companiesoffice.govt.nz/companies/app/ui/pages/companies/1842323)  
NZBN: 9429033989624

Der Auftragsverarbeiter sichert vertraglich zu, dass Ihre Daten ausschließlich an einem Standort in der Europäischen Union gespeichert und verarbeitet werden.

Um dies zu gewährleisten, verwende ich für den Versand der E-Mails ausschließlich den [spezifischen Endpunkt mail-eu.smtp2go.com](https://support.smtp2go.com/hc/en-gb/articles/4741818447129).

Durch diese technische Vorkehrung werden die per [SMTP oder API](https://support.smtp2go.com/hc/en-gb/articles/12974008254873) übermittelten Daten bis zur Übergabe an den Ihrer Domain zugeordneten E-Mail-Empfangsserver ausschließlich innerhalb der Europäischen Union verarbeitet.
