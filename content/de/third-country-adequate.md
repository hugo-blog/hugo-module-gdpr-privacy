## Übermittlung in Drittländer mit adäquatem Schutz
Ich verarbeite Daten in einem Drittland und übermittle Daten in ein Drittland oder an internationale Organisationen nur in Länder mit „adäquatem Schutz“ gemäß Artikel 45 DSGVO. Der Grund dafür ist, dass ich für Verarbeitungen in anderen Ländern die komplizierten Auflagen und Bedingungen der Artikel 44 ff. DSGVO erfüllen muss.

Die EU-Kommission kann auf der Grundlage einer „Angemessenheitsentscheidung“ beschließen, dass ein Drittland, ein Gebiet oder ein oder mehrere bestimmte Sektoren innerhalb dieses Drittlandes oder der betreffenden internationalen Organisation ein angemessenes Schutzniveau gewährleisten. In diesem Fall benötige ich als verantwortliche Person von Ihnen keine besondere Genehmigung zur Übermittlung der Daten in ein solches Drittland.

Zum Zeitpunkt der letzten Aktualisierung dieser Datenschutzbestimmungen hat die EU-Kommission für [die folgenden Länder ein angemessenes Schutzniveau festgestellt](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en):

{{< _library/hugo-module-gdpr-privacy/adequate >}}
