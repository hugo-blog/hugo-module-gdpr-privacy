## Keine Cookies

### Beschreibung und Umfang der Datenverarbeitung

_„Cookies“_ sind kleine Textdateien, die in Ihrem Internetbrowser oder von Ihrem Internetbrowser auf Ihrem Computersystem gespeichert werden.
Rufen Sie ein Online-Angebot auf, so kann ein „Cookie“ von Ihrem Betriebssystem gespeichert werden.
Dieser Cookie enthält eine charakteristische Zeichenfolge, mit der Ihr Browser und damit Sie als Person eindeutig identifiziert werden können, wenn Sie das Online-Angebot erneut aufrufen.

Mein Online-Angebot funktioniert vollständig ohne Cookies; weder werden eigene (originäre) Cookies gespeichert noch Funktionen oder Angebote Dritter verwendet, die einen zwingenden Einsatz von Cookies erfordern würden.
Daher benötige ich auch an keiner Stelle Ihr Einverständnis zur Nutzung von Cookies.
Ich untersuche mit spezieller Software, wie mein Angebot genutzt wird. Dabei bleiben Sie als einzelne Person unerkannt. Auch diese Software funktioniert ohne Cookies.

### Dauer der Speicherung, Widerspruchs- und Beseitigungsmöglichkeit

Cookies werden auf Ihrem Rechner gespeichert und von diesem an berechtigte Systeme übermittelt. Daher haben Sie die volle Kontrolle über die Verwendung von Cookies. Durch eine Änderung der Einstellungen in Ihrem Internetbrowser können Sie die Übertragung von Cookies deaktivieren oder einschränken. Bereits gespeicherte Cookies können Sie jederzeit löschen. Sie können Cookies auch automatisch löschen lassen, wenn Sie den Browser schließen oder den Rechner ausschalten.
