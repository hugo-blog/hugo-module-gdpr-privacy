### Content Delivery Network CDNsun

Zur Auslieferung meines Online-Angebots bediene ich mich eines „Content Delivery Networks“.
Dabei werden meine Inhalte weltweit auf die Server des Auftragsverarbeiters CDNsun übertragen und von dort an Sie ausgeliefert.

Der Dienst [CDNsun](https://cdnsun.com/terms-and-conditions) wird angeboten von

CDNsun s.r.o.  
V zářezu 902/4  
Prag, 158 00  
Tschechische Republik
    
Registerbehörde: [Ministerstvo spravedlnosti České republiky](https://or.justice.cz/ias/ui/rejstrik)  
Unternehmensnummer: [05745314](https://or.justice.cz/ias/ui/rejstrik-firma.vysledky?subjektId=960587&typ=PLATNY)

Ich habe die Standorte für mein Online-Angebot so ausgewählt und beschränkt, dass die Inhalte nur aus EU-Mitgliedstaaten und solchen Nicht-EU-Ländern ausgeliefert werden, die aus Sicht der EU-Kommission ein vergleichbares Schutzniveau bieten.
Ich lasse meine Inhalte von folgenden Standorten aus ausliefern:

{{< _library/hugo-module-gdpr-privacy/pointsOfPresence "cdnsun" >}}
