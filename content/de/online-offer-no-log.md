## Bereitstellung des Online-Angebots und Erstellung von Logdateien

### Beschreibung und Umfang der Datenverarbeitung

Wenn Sie mein Online-Angebot aufrufen oder nutzen, erfassen meine Systeme oder die Systeme meiner Auftragsverarbeiter automatisiert Daten und Informationen vom Computersystem des aufrufenden Rechners. Folgende Daten werden hierbei erhoben:
1. Informationen über Ihren Browsertyp und die verwendete Version
1. Das Betriebssystem Ihres Rechners
1. Ihre IP-Adresse
1. Datum und Uhrzeit des Zugriffs
1. Websites, von denen aus Ihr System auf mein Online-Angebot gelangt (eingehende Verknüpfungen)
1. Die Präferenz der natürlichen Sprache, in der Sie mein Online-Angebot nutzen möchten

Diese Daten werden ausschließlich in flüchtigem Speicher für die Dauer der Anfrage gehalten und nicht in Logdateien oder anderem dauerhaften Speicher abgelegt.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die vorübergehende Speicherung der Daten ist Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Zweck der Datenverarbeitung

Ihre IP-Adresse muss durch das System vorübergehend und flüchtig gespeichert werden, um das Online-Angebot an Ihren Rechner auszuliefern. Hierfür muss Ihre IP-Adresse für die Dauer der Anfrage in flüchtigem Speicher verarbeitet werden.

Sie können dieser Verarbeitung technisch bedingt nicht ausdrücklich zustimmen. Deshalb stützt sich der Zweck dieser Datenverarbeitung auf mein berechtigtes Interesse daran, dass Sie mein Angebot nutzen und damit auf Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Wurden die Daten zum Zweck der Bereitstellung des Online-Angebots erfasst, ist dies der Fall, sobald die jeweilige Anfrage abschließend verarbeitet wurde.

### Widerspruchs- und Beseitigungsmöglichkeit

Die Erfassung der Daten zur Bereitstellung des Online-Angebots und die flüchtige Speicherung während der Verarbeitung der Anfrage ist für den Betrieb des Online-Angebots unverzichtbar. Sie können der Speicherung und Verarbeitung daher nicht widersprechen, wenn Sie mein Angebot nutzen. Sofern Sie die Verarbeitung dieser Daten nicht wünschen, dürfen Sie mein Angebot nicht weiter nutzen.
