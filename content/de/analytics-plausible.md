## Analyse der Nutzung

### Beschreibung und Umfang der Datenverarbeitung

Wenn Sie wünschen, dass ich nicht auswerte, welche Teile meines Angebots Sie nutzen, können Sie dies über technische Einstellungen signalisieren. Dazu konfigurieren Sie Ihren Internetbrowser so, dass ein [„Global Privacy Control“](https://globalprivacycontrol.org/) oder [„do-not-track“](https://de.wikipedia.org/wiki/Do_Not_Track_(Software)) Signal mit jeder Anfrage übermittelt wird.
Sofern Ihr Browser weder [„do-not-track“](https://de.wikipedia.org/wiki/Do_Not_Track_(Software)) noch [„Global Privacy Control“](https://globalprivacycontrol.org/) Information übermittelt, leiten meine Systeme oder die Systeme der in meinem Auftrag verarbeitenden Unternehmen den Aufruf zu einer [datenschutzfreundlichen Analyse-Plattform](https://plausible.io/) weiter.
Hierbei erhebe ich folgende Daten:
- _Seiten-URL_ – Ich verfolge die Seiten-URL jeder Seitenansicht meines Online-Angebots, um zu erfahren, welche Seiten mein Publikum ansieht und wie oft eine bestimmte Seite angesehen wurde.
- _HTTP Referer_ – Ich verwende eingehende Verweise, um die Anzahl der Aufrufe zu erfassen, die von Links auf anderen Websites auf mein Online-Angebot verwiesen wurden.
- _Browser_ – Ich werte aus, welche Browser Sie beim Besuch meiner Website verwenden.
  Diese Information wird aus technischen Protokolldaten abgeleitet (dem Feld „User-Agent“ in den HTTP-Kopfdaten).
  Die vollständigen Protokolldaten werden verworfen.
- _Betriebssystem_ – Ich erfasse ausschließlich die Marke des Betriebssystems und speichere weder die Versionsnummer noch andere Details.
  Diese Information wird aus technischen Protokolldaten abgeleitet (dem Feld „User-Agent“ in den HTTP-Kopfdaten).
  Die vollständigen Protokolldaten werden verworfen.
- _Gerätetyp_ – Ich werte aus, mit welchen Geräten Sie mein Online-Angebot nutzen, damit ich die Darstellung für diese Geräte optimieren kann.
  Diese Information wird aus der Anzahl der verfügbaren Bildpunkte in der Breite des Browsers abgeleitet (Parameter window.innerWidth).
  Die tatsächliche Breite des Browsers in Bildpunkten (Pixeln) wird verworfen.
- _Besucherland_ – Anhand der IP-Adresse schlage ich das Land nach, aus dem Sie mein Online-Angebot nutzen.
  Ich speichere keine genauere Information als das Herkunftsland.
  Ihre IP-Adresse wird verworfen.
  Ich speichere niemals IP-Adressen in meinen Analysedatenbanken oder Analyseprotokollen.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die vorübergehende Speicherung der Daten ist Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Zweck der Datenverarbeitung

Ich verwende die moderne, datenschutzfreundliche Analyse-Software [plausible.io](https://plausible.io/), um Ihre Nutzung meines Online-Angebots _vollständig anonymisiert_ zu untersuchen. Dies hilft mir dabei, zu verstehen, wie Sie mein Online-Angebot nutzen. Nur so kann ich Ihnen dauerhaft nützliche Inhalte anbieten.

Insbesondere werden [_keine Cookies_ verwendet, um Ihr persönliches Verhalten nachzuverfolgen](https://plausible.io/privacy). Daher frage ich Sie auch nicht, ob Sie einverstanden sind, dass ich Cookies verwende.

Bei diesen Analysen interessiert mich nur das statistisch relevante Verhalten meines Publikums als Personengesamtheit, nicht das Verhalten einzelner Personen. Daher verwende ich eine Analyse-Plattform, die darauf ausgelegt ist, _keine_ personenbezogenen Daten zu erfassen und die darauf abzielt, alle möglicherweise auf einzelne Personen beziehbaren Daten zu anonymisieren. Diese Analysen benötige ich, um den Service für Sie weiter zu verbessern. Ihre Daten werden nur dazu verwendet, die Inhalte meines Online-Angebots zu verbessern und Ihnen zu helfen, die gewünschten Informationen zu finden.

In diesen Zwecken liegt auch mein berechtigtes Interesse an der Datenverarbeitung nach Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Im Falle der Erfassung der Daten zu Analyse-Zwecken ist dies frühestens nach Ablauf eines Jahres der Fall.

### Widerspruchs- und Beseitigungsmöglichkeit

Ich verwende die Analyse-Plattform nur, wenn Ihr Browser weder eine „Global Privacy Control“ noch eine „do-not-track“ Information übermittelt. Obwohl einige Stimmen den „do-not-track“ Mechanismus als „gescheiterten Versuch“ betrachten, unterstützen nach wie vor gängige Browser diese Funktionalität. Ich bin bemüht, ähnliche Mechanismen zukünftig ebenfalls einzusetzen, sofern diese ausreichend Verbreitung finden. Sie werden in der Fußzeile der Seiten meines Online-Angebots deutlich sichtbar darüber informiert, ob Ihr Browser die „do-not-track“ Funktionalität für mein Online-Angebot aktiviert hat. Wenn Sie Ihren „do-not-track“ Wunsch technisch durchsetzen möchten, empfehle ich die Installation eines Browser-Plug-ins wie [Privacy Badger](https://privacybadger.org/de/).

Da ich Sie in den Auswertungen und Analysen nicht eindeutig als Person identifizieren kann, habe ich auch keine Möglichkeit, spezifische Daten über Sie zu löschen oder zu sperren.
