## Bereitstellung des Online-Angebots und Erstellung von Logdateien

### Beschreibung und Umfang der Datenverarbeitung

Wenn Sie mein Online-Angebot aufrufen oder nutzen, erfassen meine Systeme oder die Systeme der im Auftrag verarbeitenden Unternehmen automatisiert Daten und Informationen vom Computersystem des aufrufenden Rechners. Folgende Daten erhebe ich hierbei:
1. Informationen über Ihren Browsertyp und die verwendete Version
1. Das Betriebssystem Ihres Rechners
1. Ihre IP-Adresse
1. Datum und Uhrzeit des Zugriffs
1. Websites, von denen aus Ihr System auf mein Online-Angebot gelangt (eingehende Verknüpfungen)
1. Die Präferenz der natürlichen Sprache, in der Sie mein Online-Angebot nutzen möchten

Meine Systeme oder die Systeme der im Auftrag verarbeitenden Unternehmen speichern diese Daten ebenfalls in Logdateien.

### Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die vorübergehende Speicherung der Daten ist Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Zweck der Datenverarbeitung

Ihre IP-Adresse muss durch das System vorübergehend und flüchtig gespeichert werden, um das Online-Angebot an Ihren Rechner auszuliefern. Hierfür muss Ihre IP-Adresse für die Dauer der Anfrage in flüchtigem Speicher verarbeitet werden.

Sie können dieser Verarbeitung technisch bedingt nicht ausdrücklich zustimmen. Darum rechtfertigt mein berechtigtes Interesse daran, dass Sie mein Online-Angebot nutzen, den Zweck dieser Verarbeitung. Rechtsgrundlage ist folglich Artikel 6 Absatz 1 Buchstabe f DSGVO.

### Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Bei der Erfassung der Daten zur Bereitstellung des Online-Angebots ist dies der Fall, wenn die jeweilige Anfrage abschließend verarbeitet wurde.

### Widerspruchs- und Beseitigungsmöglichkeit

Die Erfassung der Daten zur Bereitstellung des Online-Angebots und die Speicherung der Daten in Logdateien ist für den Betrieb des Online-Angebots unverzichtbar. Sie können der Speicherung und Verarbeitung daher nicht widersprechen, wenn Sie mein Angebot nutzen. Wenn Sie die Verarbeitung dieser Daten nicht wünschen, dürfen Sie mein Angebot nicht weiter nutzen.
