## Verantwortliche Person

Verantwortliche Person (im Gesetzestext: „Verantwortlicher“) im Sinne der [EU-Datenschutzgrundverordnung (DSGVO)](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32016R0679), anderer nationaler Datenschutzgesetze der Mitgliedstaaten sowie sonstiger datenschutzrechtlicher Bestimmungen ist: