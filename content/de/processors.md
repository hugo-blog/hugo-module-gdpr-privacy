## Zusammenarbeit mit Auftragsverarbeitern und Dritten
Soweit ich im Rahmen meiner Verarbeitung Daten gegenüber anderen Personen, Unternehmen oder sonstigen Dritten offenbare, sie an diese übermittle oder ihnen sonst Zugriff auf die Daten gewähre, geschieht dies nur auf der Grundlage

- Ihrer Einwilligung
- einer gesetzlichen Erlaubnis
- einer rechtlichen Verpflichtung oder
- meiner berechtigten Interessen.

Wenn meine eigenen Interessen betroffen sind, werde ich vor jeder Weitergabe Ihrer Daten sorgfältig abwägen und versuchen, meine Interessen mit Ihren Rechten und Interessen in Einklang zu bringen.

Soweit ich Dritte auf Grundlage eines Auftragsverarbeitungsvertrages mit der Datenverarbeitung beauftrage, geschieht dies auf der Grundlage von Artikel 28 DSGVO.

## Auftragsdatenverarbeiter
Folgende Unternehmen habe ich mit der Verarbeitung Ihrer Daten beauftragt und mit ihnen einen Vertrag zur Auftragsdatenverarbeitung geschlossen:
