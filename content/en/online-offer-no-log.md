## Provision of the on-line offering and creation of log files

### Description and scope of data processing

With each call for the use of my on-line offering, my systems, or the systems of my processors, automatically record data and information from the computer system of the calling computer. The following data is collected:
1. Information about your browser type and version used
1. The operating system of your computer
1. Your IP address
1. Date and time of access
1. Websites from which your system accesses my on-line offering (incoming links)
1. The preference of the natural language in which you would like to use my on-line offering

This data is kept only in volatile memory for the duration of the request and is not kept in log files or other persistent storage.

### Legal basis for data processing

The legal basis for the temporary storage of data is Article 6(1) point (f) of the GDPR.

### Purpose of data processing

Your IP address must be temporarily held in volatile storage by the system to deliver the on-line offering to your computer. For this purpose, your IP address must be processed in volatile memory for the duration of the request.

For technical reasons, you cannot explicitly consent to this processing. Therefore, the purpose of this data processing is based on my legitimate interest in you using my offer and thus on Article 6(1) point (f) GDPR.

### Duration of storage

The data will be deleted as soon as they are no longer necessary for the purpose for which they were collected. If the data was collected to provide the on-line offer, this is the case as soon as the respective request has been finally processed.

### Possibility of objection and removal

The collection of the data for the provision of the on-line offering and the volatile storage during the processing of the request is indispensable for the operation of the on-line offering. Therefore, you cannot object to the storage and processing if you use my offer. If you do not wish these data to be processed, you must not use my offer any further.
