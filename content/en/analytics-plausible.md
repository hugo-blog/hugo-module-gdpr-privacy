## Analysis of use

### Description and scope of data processing

If you do not want me to evaluate which parts of my offer you use, you can signal this via technical settings. To achieve this, configure your Internet browser to transmit a [“global privacy control”](https://globalprivacycontrol.org/) or [“do-not-track”](https://en.wikipedia.org/wiki/Do_Not_Track) signal with each request.
Unless your browser transmits [“do-not-track”](https://en.wikipedia.org/wiki/Do_Not_Track) or [“Global Privacy Control”](https://globalprivacycontrol.org/) information, my systems, or my processors' systems will route the call to a [privacy-friendly analysis platform](https://plausible.io/).
In doing so, I collect the following data:
- _Page URL_ – I track the page URL of each page view of my on-line offering to learn which pages my audience is viewing and how many times a particular page has been viewed.
- _HTTP Referer_ – I use inbound links to track the number of visitors that were referred to my on-line offering from links on other websites.
- _Browser_ – I evaluate, which browsers you use when you visit my website.
   This information is derived from technical protocol data (the User-Agent field in the HTTP header data).
   The full protocol data is discarded.
- _Operating System_ – I only record the brand of the operating system and do not store the version number or other details.
   This information is derived from technical protocol data (the User-Agent field in the HTTP header data).
   The full protocol data is discarded.
- _Device type_ – I evaluate, with which devices you use my on-line offering, to optimize the display for these devices.
   This information is derived from the number of available pixels in the width of the browser (window.innerWidth parameter).
   The real width of the browser in pixels is discarded.
- _Visitor country_ – Based on the IP address, I look up the country from which you are using my on-line offering.
   I do not store information more specific than the country of origin.
   Your IP address will be discarded.
   I never store IP addresses in my analysis databases or analysis logs.

### Legal basis for data processing

The legal basis for the temporary storage of data is Article 6(1) point (f) of the GDPR.

### Purpose of data processing

I use the modern, data protection-friendly analysis software [plausible.io](https://plausible.io/) to analyse your use of my on-line offering in a _completely anonymous way_. This helps me understand how you use my on-line offering. This is the only way I can provide you with useful content permanently.

In particular, [_no cookies_ are used to track your personal behaviour](https://plausible.io/privacy). Therefore, I do not ask you if you agree that I use cookies.

In these analyses, I am only interested in the statistically relevant behaviour of all users as a group, not the behaviour of individual users. Therefore, I use an analysis platform that is designed _not_ to collect any personal data and that aims to make anonymous all data that could relate to individual persons. I need these analyses to further improve the service for you. Your data will only be used to improve the user experience of my on-line offering and to help you find the information you are looking for.

These purposes also include my legitimate interest in data processing in accordance with Article 6(1) point (f) GDPR.

### Duration of storage

The data will be deleted as soon as they are no longer necessary for the purpose for which they were collected. When collecting data for analysis purposes, this shall not take place before one year has elapsed.

### Possibility of objection and removal

I only use the analysis platform if your browser does not transmit either “global privacy control” or “do-not-track” information. Although some voices regard the “do-not-track” mechanism as a “failed attempt”, popular browsers still support this functionality. I am endeavouring to use similar mechanisms in the future, provided that they are sufficiently widespread. In the footer of the pages of my on-line offering, you will be informed visibly whether your browser has activated the “do-not-track” functionality for my on-line offering. If you want to technically enforce your “do-not-track” request, I recommend installing a browser plug-in like [Privacy Badger](https://privacybadger.org/).

Since I cannot identify you as a person in the evaluations and analyses, I also have no possibility to delete or block specific data about you.
