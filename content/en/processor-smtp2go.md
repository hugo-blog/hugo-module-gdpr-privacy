### Mailing

I use a professional mailing company to send personal and transactional emails.

The [SMTP2GO](https://www.smtp2go.com/about/) service is offered by

SAND DUNE MAIL LIMITED  
96-106 Manchester Street  
Christchurch 8011  
New Zealand

Register authority: [New Zealand Companies Office](https://www.companiesoffice.govt.nz/)  
Company number: [1842323](https://app.companiesoffice.govt.nz/companies/app/ui/pages/companies/1842323)  
NZBN: 9429033989624

The processor contractually ensures that your data is stored and processed exclusively at a location in the European Union.

To ensure this, I only use the [specific endpoint mail-eu.smtp2go.com](https://support.smtp2go.com/hc/en-gb/articles/4741818447129)to send the emails.

Through this technical measure, the data transmitted through [SMTP or API](https://support.smtp2go.com/hc/en-gb/articles/12974008254873) is processed exclusively within the European Union until it is transferred to the email receiving server assigned to your domain.
