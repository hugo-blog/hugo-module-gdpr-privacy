### Newsletter dispatch

I use a professional, multi-certified mailing service for the regular dispatch of my email newsletter.

The service [rapidmail](https://www.rapidmail.de/) is offered by

rapidmail GmbH  
Augustinerplatz 2  
79098 Freiburg im Breisgau  
Germany

Register court: Freiburg i.Br  
Registration number: HRB 706983  
EU VAT. ID: DE262810345

The processor contractually guarantees that your data will be stored and processed exclusively at a location in Germany.
