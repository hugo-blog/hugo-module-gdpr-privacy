## Introduction

It is important to me to protect your personal data and to process it securely.
As a matter of course, I adhere to the provisions of the relevant data protection laws, in particular the [EU General Data Protection Regulation (GDPR)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32016R0679).
In the following text, I will refer to your personal data simply and briefly as “data”. Personal data are all data with which you can be personally identified.
All linked websites, functions, and content that I offer you on-line, I refer to collectively as “on-line offering” for short.

I explain to you how I process your data when you use my on-line offer.

Basically, I support the principle of data minimization. I therefore only collect and process data for clear and specifically named purposes. I make these purposes visible for you. Whenever it is technically possible, I ask for your consent before processing your data.
You can use my offer incognito within the scope of what is technically possible.
Your personal data will be processed if it is factually or technically necessary. In addition, your data will be processed if a law requires me to do so.
