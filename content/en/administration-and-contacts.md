## Administration, accounting, contact management

### Description and scope of data processing

Within the scope of administrative tasks as well as for the organization of my company, accounting and for compliance with legal obligations (such as, among others, archiving), I process data exclusively outside my on-line offering. In this context, I process the same data that I process for the provision of the professional services contracted.

In this context, I disclose or transfer data to the tax authorities, consultants (such as tax consultants or auditors) as well as other fee agencies, public authorities and payment service providers.

Furthermore, I store information on suppliers, other business partners and potential business partners based on my business interests.

Data subjects can be my customers, interested parties, suppliers, potential business partners as well as their customers, users, website visitors or employees and third parties.

### Legal basis for data processing

When I process data to carry out my business activities or to provide contracted services, the legal bases of the processing are Article 6(1) point (c) GDPR and Article 6(1) point (f) GDPR.

### Purpose of data processing

The purpose and my interest in processing lies in the administration, accounting and archiving of data.
These are tasks that I perform to maintain my business. This includes some legal obligations that I have to fulfil. Moreover, these are partly tasks with which I can provide my services.

### Duration of storage

I store most of the data related to companies permanently.
To fulfil legal archiving obligations, I delete the data at the earliest after the expiry of the legal retention periods and at the latest one year after the expiry of these periods.

### Possibility of objection and removal

You can send an objection in writing to my postal address as the controller.
In most cases, my interest as controller in the processing is likely to outweigh your request for deletion.
In such cases, the data is regularly blocked instead of deleted.
Notwithstanding this, the general rules on data deletion and blocking shall apply.
