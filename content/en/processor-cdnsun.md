### Content Delivery Network CDNsun

I use a content delivery network to deliver my on-line offering.
My content is distributed worldwide on the servers of CDNsun (processor) and delivered to you from there.

The [CDNsun](https://cdnsun.com/terms-and-conditions) service is offered by

CDNsun s.r.o.  
V zářezu 902/4  
Prague, 158 00  
Czech republic

Register authority: [Ministerstvo spravedlnosti České republiky](https://or.justice.cz/ias/ui/rejstrik)  
Company number: [05745314](https://or.justice.cz/ias/ui/rejstrik-firma.vysledky?subjektId=960587&typ=PLATNY)

I have selected or restricted the locations for my on-line offering in such a way that the content is only delivered from EU member states and from non-EU countries that offer adequate protection from the perspective of the EU Commission.
I have my content delivered from the following locations:

{{< _library/hugo-module-gdpr-privacy/pointsOfPresence "cdnsun" >}}
