## No cookies

### Description and scope of data processing

_“Cookies”_ are small text files that are stored in your Internet browser or by your Internet browser on your computer system.
If you call up an on-line offering, a “cookie” may be stored on your operating system.
This cookie contains a characteristic string of characters that enables the browser and thus you as a user to be uniquely identified when you revisit the on-line offering.

My on-line offering functions completely without cookies; neither my own (original) cookies are stored nor are functions or offers of third parties used which would require the mandatory use of cookies.
Therefore, I do not require your consent to the use of cookies at any point.
I use special software to examine how my visitors use my offer. In doing so, you remain unrecognized as an individual person. This software also works without cookies.

### Duration of storage, possibility of objection and removal

Cookies are stored on your computer and sent by it to authorized systems. Therefore, you have full control over the use of cookies. By changing the settings in your network browser, you can deactivate or restrict the transmission of cookies. You can delete cookies that have already been saved at any time. You can also have cookies deleted automatically when you close the browser or turn off the computer.
