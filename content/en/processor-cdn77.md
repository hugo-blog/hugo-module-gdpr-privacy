### Content Delivery Network CDN77

I use a content delivery network to deliver my on-line offering.
My content is distributed worldwide on the servers of CDN77 (processor) and delivered to you from there.

The [CDN77](https://www.cdn77.com/contact) service is offered by

DataCamp Limited  
207 Regent Street  
London W1B HH  
United Kingdom

Register authority: [Companies House](https://www.gov.uk/government/organisations/companies-house)  
Company number: [07489096](https://find-and-update.company-information.service.gov.uk/company/07489096)

I have selected or restricted the locations for my on-line offering in such a way that the content is only delivered from EU member states and from non-EU countries that offer adequate protection from the perspective of the EU Commission.
I have my content delivered from the following locations:

{{< _library/hugo-module-gdpr-privacy/pointsOfPresence "cdn77" >}}
