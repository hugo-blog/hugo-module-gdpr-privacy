## Business consulting and training services

### Description and scope of data processing

I process exclusively outside my on-line offering the business-related data of my customers within the scope of my contractual services, which include conceptual and strategic consulting, software and design development, software consulting, implementation of software and processes, data analysis, consulting services and training services.

Here I process
1. Inventory data (for example customer master data, such as names or addresses)
1. Contact details (for example, email, telephone numbers)
1. Content data (such as source code, texts, drawings, diagrams, photographs, videos)
1. Contract data (for example, subject matter of the contract, duration)
1. Payment data (for example bank details, payment targets, payment history)
1. Usage and meta-data (for example, in the context of evaluating and measuring the success of marketing measures)

As a matter of principle, I do not process special categories of personal data, unless these are explicitly part of a commissioned processing.

### Legal basis for data processing

Data subjects can be my customers, interested parties, suppliers, potential business partners as well as their customers, users, website visitors or employees and third parties.

In the processing of personal data necessary for the performance of a contract to which you as a data subject are party, Article 6(1) point (b) GDPR constitutes the legal basis.

For the purposes of analysis, statistics, optimization and the implementation of security measures, the legal basis is Article 6(1) point (f) GDPR.

#### Purpose of data processing

The purpose of the processing is to provide contractual services, billing and customer service.

I process data that are necessary for the justification and fulfilment of the contractual services and point out the need to provide them. Disclosure to third parties will only be made if it is necessary within the scope of an order. When processing the data provided to me in the context of an order, I act in accordance with the instructions of my clients as well as the legal requirements of an order processing in accordance with Article 28 GDPR and do not process the data for any other purposes than those specified in the order.

### Duration of storage

I delete the data after the expiry of legal warranty and comparable obligations.
I review the necessity every two years.
If data is subject to legal archiving obligations, I will delete it at the end of the legal retention period (6 years according to § 257 (1) HGB, 10 years according to § 147 (1) AO).
If you have disclosed data to me in the context of a business order, I will delete the data in accordance with the specifications of the order, generally after the end of the order.

### Possibility of objection and removal

If you, as the client, object to the processing of the data necessary for the provision of my contractual service, I cannot continue the business relationship with you. Notwithstanding this, the general rules on data deletion and blocking shall apply.
