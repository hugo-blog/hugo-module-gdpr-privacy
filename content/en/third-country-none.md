## No transfer to non-EU jurisdiction or to international organizations
Due to the complicated requirements and conditions of Articles 44 et seq. GDPR, there is neither processing of data in a non-EU jurisdiction nor transfer of data to a non-EU jurisdiction nor to international organizations.
