### Content Delivery Network KeyCDN

I use a content delivery network to deliver my on-line offering.
My content is distributed worldwide on the servers of KeyCDN (processor) and delivered to you from there.

The [KeyCDN](https://www.keycdn.com/about) service is offered by

proinity LLC  
Reichenauweg 1  
8272 Ermatingen  
Switzerland

Register authority: [Federal Statistical Office](https://www.bfs.admin.ch/bfs/de/home/register/unternehmensregister.html)  
UID: [CHE-459.847.656](https://www.uid.admin.ch/Detail.aspx?uid_id=CHE-459.847.656)

I have selected or restricted the locations for my on-line offering in such a way that the content is only delivered from EU member states and from non-EU countries that offer adequate protection from the perspective of the EU Commission.
I have my content delivered from the following locations:

{{< _library/hugo-module-gdpr-privacy/pointsOfPresence "keycdn" >}}
