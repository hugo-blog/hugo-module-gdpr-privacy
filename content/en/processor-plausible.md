### Analysis Platform

I have commissioned a provider of a data protection-friendly analysis platform with the completely anonymized evaluation of the use of my offer.
Your data will not be transferred there if your browser sends a [“do-not-track”](https://en.wikipedia.org/wiki/Do_Not_Track) or [“Global Privacy Control”](https://globalprivacycontrol.org/) signal.

The service [plausible.io](https://plausible.io/) is offered by

Plausible Insights OÜ  
Västriku tn 2  
50403 Tartu  
Estonia

Register authority: [e-Business Register](https://www.rik.ee/en/e-business-register)  
Register number: [14709274](https://ariregister.rik.ee/eng/company/14709274/O%C3%9C-Plausible-Insights)

The processor stores and processes the anonymized data in a data centre in [Frankfurt (Main), Germany](https://plausible.io/blog/made-in-eu).
