## Controller

The controller within the meaning of the [EU General Data Protection Regulation (GDPR)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32016R0679), other national data protection laws of the member states and other data protection regulations: