## Cooperation with processors and third parties
If I disclose, transfer or otherwise grant access to data to other persons and companies (processors or third parties) in the course of my processing, this will only be done based on

- Your consent
- A legal permission
- A legal obligation, or
- My legitimate interests.

If my own interests are involved, I will carefully consider and try to balance my interests with your rights and interests before any disclosure of your information.

If I commission third parties to process data in scope of a data processing agreement, this is done based on Article 28 GDPR.

## Processors
I have commissioned the following companies to process your data and have concluded a contract with them for commissioned data processing:
