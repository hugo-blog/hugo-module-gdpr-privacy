## No-nonsense Executive Summary

Strict data protection regulations apply within the European Union (EU).
Therefore, I need to explain to you exactly how I process your data.
I will also explain your rights in this regard.
To save time, please read the summary first:

- Accountable (“controller”) for this on-line offering in the sense of the EU-GDPR and national legislation is {{< meta/full-name >}}.
- I welcome the EU GDPR legislation and the principle of data minimization.
- I examine how my visitors use my offer.
   In doing so, you remain unrecognized as an individual person.
   I do not use any technique that would allow me to observe your personal behaviour. I do not use so-called _cookies_.
- You will support me if you allow this modern form of analysis
- You can voluntarily send me emails.
   You can also subscribe to my newsletter without coercion or obligation.
   You can cancel the newsletter at any time.
- I will only share your information with third parties if I am required to do so by law or valid court order.
- Your data will only be processed within the EU or under non-EU jurisdiction with [adequate protection](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en).
