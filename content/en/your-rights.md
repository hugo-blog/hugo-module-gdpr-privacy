## Rights of the data subject
If your personal data is processed, you are a data subject in the sense of the GDPR and you have the following rights against me, the controller:

### Right of access by the data subject
In accordance with Article 15 of the GDPR, you can request confirmation from me, the controller, whether I am processing personal data concerning you.
If such processing happens, you may request the following information from the controller:

1. The purposes for which the data are processed
1. The categories of personal data processed
1. The recipients or categories of recipients to whom the data concerning you have been or will be disclosed
1. The planned duration of storage of data relating to you or, if it is not possible to give details, criteria for determining the duration of storage
1. The existence of a right of rectification or erasure of data concerning you, a right to restriction of processing by the controller or a right to object to such processing
1. The existence of a right of appeal to a supervisory authority pursuant to Article 77 GDPR
1. All available information about the origin of the data, if the data is not collected directly from you as the data subject

You may request information whether I transfer your data to a non-EU jurisdiction or to an international organization.
In connection with this transmission, you may request to be informed of the appropriate guarantees in accordance with Article 46 GDPR.

### Right of rectification
Under Article 16 of the GDPR, you have the right to ask the controller to correct and complete the processed personal data concerning you if it is incorrect or incomplete.
The controller shall make the correction without undue delay.

### Right to erasure
_Obligation to delete_  
In accordance with Article 17 GDPR, you can demand that I, the controller, delete your data immediately, and I am obliged to delete this data immediately if one of the following reasons applies:

1. The data concerning you are no longer necessary for the purposes for which they were collected or otherwise processed.
1. You withdraw your consent on which the processing was based under Article 6(1) point (a) or Article 9(2) point (a) GDPR and there is no other legal basis for the processing.
1. You object to the processing in accordance with Article 21(1) GDPR and there are no overriding legitimate reasons for the processing, or you object to the processing in accordance with Article 21(2) GDPR.
1. The data concerning you have been processed unlawfully.
1. The controller is obliged to delete the data concerning you in accordance with Union law or the law of the Member States to which it is subject.
1. The data concerning you have been collected relating to information society services offered in accordance with Article 8(1) GDPR.

_Information to third parties_  
If I, the controller, have made public the data concerning you and I am obliged to delete them pursuant to Article 17(1) GDPR, I shall take reasonable measures. These can also be purely technical measures.
With the measures, I inform the other controllers within the meaning of the GDPR for the processing of your data that you, as the data subject, have requested them to delete all links to these data or copies or replications of these data.

_Exceptions_  
You do not have the right to erasure if the processing is necessary

1. On the exercise of the right to freedom of expression and information
1. To comply with a legal obligation imposed on the controller under Union or national law to which the controller is subject or in the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller
1. For reasons of public interest in the field of public health pursuant to Article 9(2) points (h) and (i) as well as Article 9(3) GDPR
1. For archiving, scientific or historical research purposes in the public interest or for statistical purposes pursuant to Article 89 (1) GDPR, insofar as the law referred to in Section 1) is likely to render impossible or seriously prejudice the attainment of the objectives of such processing, or
1. To assert, exercise or defend legal claims.

Unless the data are deleted because they are required for other and legally permissible purposes, their processing is restricted.
This means that the data will be blocked and not processed for other purposes.
This applies, for example, to data that must be retained for commercial or tax law reasons.  
According to legal requirements in Germany, data must be retained for 10 years in accordance with Section 147 (1) of the German Fiscal Code (AO) and Section 257 (1) Nos. 1 and 4, (4) of the German Commercial Code (HGB) (books, records, management reports, accounting vouchers, commercial ledgers, documents relevant for taxation and similar) and for 6 years in accordance with Section 257 (1) Nos. 2 and 3, (4) of the HGB (commercial letters).
**Emails constitute commercial letters if they have been exchanged in the context of a business relationship between you, the data subject, and me, the controller.**

### Right to restriction of processing
If at least one of the following conditions is met, you may request the restriction of the processing of personal data concerning you in accordance with Article 18 GDPR:

1. If you dispute the accuracy of the data concerning you for a period that allows the controller to verify the accuracy of the data.
1. The processing is unlawful, and you object to the deletion of the personal data and request instead the restriction of the use of the personal data.
1. The controller no longer needs the personal data for the purposes of the processing, but you need it to exercise or defend your rights.
1. You have objected to the processing under Article 21(1) of the GDPR and it has not yet been definitively established whether the controller's legitimate grounds outweigh your grounds.

Where the processing of data relating to you has been restricted, such data may be processed (except storage) only with your consent or to pursue, exercise or defend legal claims or to protect the rights of another natural or legal person or on grounds of an important public interest of the Union or of a Member State.
If the restriction on processing has been restricted in accordance with the above conditions, you shall be informed by the controller before the restriction is lifted.

### Notification obligation
If you have asserted the right to rectification, erasure, or restriction of processing towards the controller, the latter is obliged, pursuant to Article 19 GDPR, to notify all recipients of such rectification, erasure, or restriction of processing to whom the data concerning you have been disclosed, unless this proves impossible or involves a disproportionate effort.  
You are entitled to be informed of these recipients by the controller.

### Right to data portability
Pursuant to Article 20 GDPR, you are entitled to receive the data concerning you that you have provided to the controller in a structured, common and machine-readable format.
You also are entitled to have this data transferred to another controller without interference from the controller to whom the data has been made available, provided that

1. The processing is based on a consent pursuant to Article 6(1) point (a) GDPR or Article 9(2) point (a) GDPR or on a contract pursuant to Article 6(1) point (b) GDPR and
1. The processing is carried out by automated procedures.

In exercising this right, you also are entitled to have the data concerning you transferred directly from one controller to another, as far as this is technically feasible.  
The freedoms and rights of other persons may not be impaired thereby.  
The right to data portability shall not apply to processing of data which is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller.

### Right to object
Pursuant to Article 21 GDPR, you are entitled to object at any time, for reasons arising from your particular situation, to the processing of data concerning you based on Article 6(1) points (e) or (f) GDPR, including profiling based on these provisions.  
In this case, the controllers may no longer process the data concerning you, unless they can demonstrate compelling reasons for processing which are justified on grounds of protection of your interests, rights and freedoms, or if the processing serves to assert, exercise or defend legal claims.
