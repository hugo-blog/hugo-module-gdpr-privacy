## General information on data processing

### Scope of the processing of personal data

As a matter of principle, I only process your data so that you can use my on-line offering and so that I can present my content and provide my services.
I regularly process your data only if you consent in advance.
An exception applies if you cannot consent in advance for actual reasons and legal regulations allow me to process the data anyway.

### Legal basis for the processing of personal data

As a data subject, you can consent to me processing your data.
Then Article 6(1) point (f) GDPR is the legal basis.

In the processing of personal data necessary for the performance of a contract to which you as a data subject are party, Article 6(1) point (b) GDPR constitutes the legal basis.
This also applies if I process your data to negotiate, initiate or prepare contracts with you.

In some cases, as an entrepreneur or self-employed person, I am legally obliged to process your data.
Then Article 6(1) point (c) GDPR is the legal basis.

Sometimes vital interests of the data subject or another natural person require me to process data.
Then Article 6(1) point (d) GDPR is the legal basis.

It is possible that I as a person, my company, or third parties have a legitimate interest that I process data.
If the processing is necessary to protect a legitimate interest of my person, my company, or a third party and if the interests, fundamental rights and freedoms of the data subject do not outweigh the former interest, Article 6(1) point (f) GDPR provides the legal basis for the processing.
I use this legal basis as sparingly as possible.

### Data erasure and storage duration

I block or delete the data of the data subject as soon as the purpose of storage ceases to apply.
As the controller, I am subject to European and national legislation.
I process data when this legislation provides for it in Union regulations, laws or other regulations.
I also block or delete the data when a storage period prescribed by these legal norms expires.
The data will nevertheless continue to be stored if it is necessary to conclude or fulfil a contract.
