## Email contact

### Description and scope of data processing

You can contact me using the email addresses provided.
Separately, you can subscribe to my newsletter.
I will only send you the newsletter if you have specifically requested it. If you only contact me, you will not receive my newsletter automatically.
The sending of emails on your part is just as voluntary as the subscription to my newsletter.
In this respect, by emailing me, you agree that I may use and process the data contained in the email to the full extent.
In this case, your personal data sent with the email will be processed.

I do not pass on any data to third parties in this context.
I use your data exclusively for the processing of the conversation.

When you subscribe to my newsletter, I send a link to confirm.
I send this link to the email address to which I should deliver the newsletter. You confirm that you have control over this email address when you open the link in the browser.
You also confirm that you actually want my newsletter and have ordered it yourself.
This procedure is also called “double opt-in”.
I have contracted a specialized company to send the newsletter.
There, your email address will only be used to send you the newsletter.
The company processing your data on behalf of me is strictly forbidden by contract and by law to use your data in any other way.
In particular, the company may not disclose your data to any third party.
If I learn that the company is trying to sell your data, I will file a criminal complaint, terminate the contract with the company and claim damages.

Excluded from this is a misuse of my email addresses or my newsletter offer.
In this case, I will enforce my rights.
If necessary, I will take legal action.
For this purpose, I will provide the necessary data to my legal counsel, competent police authorities, courts, tax offices and other authorized public bodies.

### Legal basis for data processing

The legal basis for the processing of data transmitted while sending an email is Article 6(1) point (f) of the GDPR.

If the email contact has the goal of concluding a contract, the additional legal basis for the processing is Article 6(1) point (b) GDPR.

If the email contact represented an abusive use of my on-line offering, the additional legal basis for processing and forwarding within the scope of the lawful judicial or extra-judicial enforcement of my interests and rights is Article 6(1) point (f) GDPR.

### Purpose of data processing

I process the data to respond to your contact, or to send you a newsletter regularly.
If you emailed me to contact me, this also indicates the necessary legitimate interest in processing the data.

### Duration of storage

The data will be deleted as soon as they are no longer necessary for the purpose for which they were collected.
For the data that you have sent by email, this is the case when the respective conversation with you has ended.
The conversation ends when it is clear from the circumstances that the matter in question has been finally clarified.
For the newsletter to be sent, this is the case if you have cancelled the subscription, or I can see that you have not actively used the subscription for more than one year.
I review the necessity every two years.
In addition, the statutory archiving obligations apply.

### Possibility of objection and removal

If you contact me by email, you can object to the storage of your personal data at any time. In such a case, I am afraid I cannot continue the conversation with you.

You can send your revocation either also by e-mail or by telephone or by letter to me, the controller. You will find my email address, telephone number and postal address at the beginning of this document.

In this case, all personal data stored during the contact will be deleted within a reasonable period after checking the legality of the deletion. If deletion is legally not possible or the legality is doubtful, it is replaced by a blocking. This is particularly the case if the contact represents an abusive use of the on-line offering, or if the contact results or could result in a legal dispute between the data subject and the controller.
