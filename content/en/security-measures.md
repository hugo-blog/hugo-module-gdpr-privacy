## Security measures
In accordance with Article 32 GDPR, I shall take appropriate technical and organizational measures to ensure a level of protection appropriate to the risk, considering state-of-the-art practices, the implementation costs and the nature, extent, circumstances, and purposes of the processing and the varying degrees of probability and seriousness of the risk to the rights and freedoms of natural persons.
The measures include in particular the safeguarding of confidentiality, integrity, and availability of data by

1. Control of the physical access to the data
1. Control of access, input, disclosure, availability assurance and disconnection of data concerning them
1. The establishment of procedures to ensure the exercise of data subject rights, deletion of data and reaction to threats to data
1. The consideration of the protection of personal data already during the development or selection of hardware, software and procedures, in accordance with the principle of data protection by designing technology and by using data protection-friendly default settings in accordance with Article 25 GDPR
