### On-line surveys

I have contracted the service provider LimeSurvey to provide on-line surveys.
If you participate in on-line surveys that I have created there, you agree to the [privacy policy](https://www.limesurvey.org/privacy-policy) and [terms and conditions](https://www.limesurvey.org/terms-conditions) of this company.
Before the survey begins, you will be explicitly asked for your consent.

The service [LimeSurvey](https://www.limesurvey.org/en) is offered by

LimeSurvey GmbH  
Survey services & consulting  
Papenreye 63  
22453 Hamburg  
Germany

Register court: Hamburg  
Registration number: HRB 137625  
EU VAT. ID: DE301233134

It has been contractually agreed with the processor that your data will be stored and processed exclusively at a location in Germany.
