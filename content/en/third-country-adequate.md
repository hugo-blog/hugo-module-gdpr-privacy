## Transfer to non-EU jurisdiction with adequate protection
I process data in non-EU jurisdiction and transfer data to non-EU jurisdiction or to international organizations only in countries with “adequate protection” according to Article 45 GDPR. The reason for this is that for processing in other countries, I have to comply with the complicated requirements and conditions of Article 44 et seq. GDPR.

The EU Commission, after assessing the adequacy of the level of protection, may decide, through an implementing act, that a non-EU jurisdiction, a territory, or one or more specified sectors within a non-EU jurisdiction, or an international organization ensures an adequate level of protection. In this case, as the controller, I do not need any special permission from you to transfer the data to such a non-EU jurisdiction.

At the time of the last update of this privacy policy, the EU Commission has [determined an adequate level of protection for the following countries](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en):

{{< _library/hugo-module-gdpr-privacy/adequate >}}
