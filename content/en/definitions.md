## Definition of terms

_“personal data”_ means any information relating to an identified or identifiable natural person. In the following, I will refer to such persons as “data subject”.
An identifiable _natural person_ is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an on-line identifier (including so-called cookies) or one or more special features.
These characteristics denote the physical, physiological, genetic, psychological, economic, cultural or social identity of a natural person.

_“Controller”_ means the natural or legal person, public authority, agency or any other body, which alone or jointly with others determines the purposes and means of the processing of personal data.

_“processing”_ means any operation or set of operations, which is performed upon personal data. It does not matter whether the operations consist of automated or manual procedures.
The term is broad and covers practically every type and form of data handling.

_“Non-EU jurisdiction”_ means any State or independent legal entity, which is not part of the European Economic Area (EEA).

_“International organization”_ means any organization which has its place of business outside the European Union or which is partly or completely under non-EU jurisdiction.

_“Processors”_ are third parties who process data on behalf of a “controller”.
Processors must follow the instructions of the controller.
The controller must conclude a separate contract on this with each processor.
These contracts must meet certain requirements of the EU GDPR.
They must be concluded in accordance with EU law or the law of an EU member state.

_“IP address”_ is a unique identifier on the network.
The service company for your Internet access assigns this identifier to you.
The temporal association between this identifier and you as a person must, under certain circumstances, be stored by the service provider for a longer period.
This may allow authorities to reconstruct your activities retrospectively as part of criminal or other investigations.
