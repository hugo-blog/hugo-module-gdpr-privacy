# Hugo GDPR Privacy Toolset

Multilingual text snippets to generate GDPR compliant privacy pages.
Translated using [OmegaT](https://omegat.org/).

**DISCLAIMER: USE AT YOUR OWN RISK. I am not a lawyer.**

This module provides

* a data list of countries that provide
  [adequate protection as defined by the EU commission](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en)
* a list of CDN points of presence in such countries
* a modular toolset to create a privacy statement as required by the GDPR

# Usage

Add this module to the list of required modules for your hugo website.

Create a `privacy.html` page that has a content similar to the following:

```
{{< library/gdpr-privacy "header" >}}
{{< library/gdpr-privacy "tldr" >}}
{{< library/gdpr-privacy "intro" >}}
{{< library/gdpr-privacy "controller" >}}
{{< library/gdpr-privacy "definitions" >}}
{{< library/gdpr-privacy "general-terms" >}}
{{< library/gdpr-privacy "online-offer-with-log" >}}
{{< library/gdpr-privacy "no-cookies" >}}
{{< library/gdpr-privacy "analytics-plausible" >}}
{{< library/gdpr-privacy "e-mail-with-newsletter" >}}
{{< library/gdpr-privacy "administration-and-contacts" >}}
{{< library/gdpr-privacy "your-rights" >}}
{{< library/gdpr-privacy "security-measures" >}}
{{< library/gdpr-privacy "third-country-adequate" >}}
{{< library/gdpr-privacy "processors" >}}
{{< library/gdpr-privacy "processor-keycdn" >}}
{{< library/gdpr-privacy "processor-cdn77" >}}
{{< library/gdpr-privacy "processor-cdnsun" >}}
{{< library/gdpr-privacy "processor-plausible" >}}
{{< library/gdpr-privacy "processor-rapidmail" >}}
{{< library/gdpr-privacy "processor-limesurvey" >}}
```

You should especially customize the processors/vendors you use and contract with.
